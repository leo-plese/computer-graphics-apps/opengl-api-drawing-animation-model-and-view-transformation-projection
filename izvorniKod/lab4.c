#include <io.h>
#include <time.h>
#include <math.h>
#include <stdio.h>
#include <stdlib.h>
#include <ctype.h>
#include <sys/timeb.h>
#include <sys/types.h>
#include <GL/glut.h>

#define PI 3.14159

/* "referenciranjem" na pojedini od sljedecih naziva mogu se koristiti
* svojstva materijala zute (Sunce), plave (Zemlja) i bijele (Mjesec)
* boje
*/
#define YELLOWMAT 1
#define BLUEMAT 2
#define WHITEMAT 3

/* varijable koje predstavljaju (kako slijedi) pomake: Zemlje oko
* svoje osi i oko Sunca, te Mjeseca oko svoje osi i oko Zemlje
*/
static double earthDay = 0, earthYear = 0, moonDay = 0, moonYear = 0;

void init(void)
{
	// definiranje komponenti lokalnog osvjetljenja pojedinih materijala 
	GLfloat yellowAmbientComp[] = { 0.1, 0.1, 0.1, 1.0 };
	GLfloat yellowDiffuseComp[] = { 0.7, 0.7, 0.0, 1.0 };
	GLfloat yellowSpecularComp[] = { 1.0, 1.0, 1.0, 1.0 };

	GLfloat blueAmbientComp[] = { 0.2, 0.2, 0.6, 1.0 };
	GLfloat blueDiffuseComp[] = { 0.1, 0.1, 0.7, 1.0 };
	GLfloat blueSpecularComp[] = { 1.0, 1.0, 1.0, 1.0 };

	GLfloat whiteAmbientComp[] = { 0.7, 0.7, 0.7, 1.0 };
	GLfloat whiteDiffuseComp[] = { 1.0, 1.0, 1.0, 1.0 };
	GLfloat whiteSpecularComp[] = { 1.0, 1.0, 1.0, 1.0 };

	// definiranje karakteristika izvora svjetlosti 
	GLfloat lightSourcePosition[] = { 1.0, 0.5, 2.0, 0.0 };
	GLfloat lightSourceDirection[] = { 0.0, 0.0, 0.0, 0.0 };

	/* pridjeljivanje svojstava materijalima (koristenjem imena YELLOWMAT
	* moze se pojedinim objektima pridjeliti definirani materijal)
	*/
	glNewList(YELLOWMAT, GL_COMPILE);
	glMaterialfv(GL_FRONT, GL_AMBIENT, yellowAmbientComp);
	glMaterialfv(GL_FRONT, GL_DIFFUSE, yellowDiffuseComp);
	glMaterialfv(GL_FRONT, GL_SPECULAR, yellowSpecularComp);
	glMaterialf(GL_FRONT, GL_SHININESS, 100.0);
	glEndList();

	glNewList(BLUEMAT, GL_COMPILE);
	glMaterialfv(GL_FRONT, GL_AMBIENT, blueAmbientComp);
	glMaterialfv(GL_FRONT, GL_DIFFUSE, blueDiffuseComp);
	glMaterialfv(GL_FRONT, GL_SPECULAR, blueSpecularComp);
	glMaterialf(GL_FRONT, GL_SHININESS, 90.0);
	glEndList();

	glNewList(WHITEMAT, GL_COMPILE);
	glMaterialfv(GL_FRONT, GL_AMBIENT, whiteAmbientComp);
	glMaterialfv(GL_FRONT, GL_DIFFUSE, whiteDiffuseComp);
	glMaterialfv(GL_FRONT, GL_SPECULAR, whiteSpecularComp);
	glMaterialf(GL_FRONT, GL_SHININESS, 80.0);
	glEndList();

	// pridjeljivanje karakteristika izvoru svjetlosti 
	glLightfv(GL_LIGHT0, GL_POSITION, lightSourcePosition);
	glLightfv(GL_LIGHT0, GL_SPOT_DIRECTION, lightSourceDirection);

	glEnable(GL_NORMALIZE);
	glEnable(GL_LIGHT0);
	glEnable(GL_LIGHTING);
	glEnable(GL_DEPTH_TEST);
}

void reshape(int w, int h)
{
	glViewport(0, 0, (GLsizei)w, (GLsizei)h);
	glMatrixMode(GL_PROJECTION);
	glLoadIdentity();

	// inicijalizacija projekcijskog volumena
	gluPerspective(80.0, (GLfloat)w / (GLfloat)h, 1.0, 40.0);

	glMatrixMode(GL_MODELVIEW);
	glLoadIdentity();
	gluLookAt(0.0, 0.0, 30.0, 0.0, 0.0, 0.0, 0.0, 1.0, 0.0);
}

// funkcija za crtanje sfere radijusa R i koraka step (udaljenosti) izmedu koncentricnih kruznica, kopirano iz predloska, 2. zad
// za manji korak (step) gustoca iscrtanih kruznica u intervalu [0, 2*PI] je veca pa je iscrtavnaje finije tj. preciznije i obratno
void drawSphere(float R, float step)
{
	int i = 0;
	float theta, fi,
		koord1[3] = { 0.0, 0.0, 0.0 },
		koord2[3] = { 0.0, 0.0, 0.0 },
		koord3[3] = { 0.0, 0.0, 0.0 },
		koord4[3] = { 0.0, 0.0, 0.0 };

	// kut fi ide kroz [0, PI] i naizmjenicno za vrijednosti i 0/1 crta jednu odnosno drugu polovicu kugle pomocu koncentricnim kruznica sa sredistem na osi koja prolazi sredistem kugle
	for (fi = 0; fi < PI; fi = fi + step)
	{
		// za trenutnu polovicu kugle iteriramo po svakoj tocki kruznice tj. kutevima iz [0, 2*PI] radi iscrtavanja cijele kruznice
		for (theta = 0; theta <= 2 * PI; theta = theta + 0.1, i = (i + 1) % 2)
		{
			if (i == 0)
			{
				koord1[0] = R * cos(theta) * sin(fi);
				koord1[1] = R * sin(theta) * sin(fi);
				koord1[2] = R * cos(fi);

				koord2[0] = R * cos(theta) * sin(fi + step);
				koord2[1] = R * sin(theta) * sin(fi + step);
				koord2[2] = R * cos(fi + step);
			}

			else if (i == 1)
			{
				koord3[0] = R * cos(theta) * sin(fi);
				koord3[1] = R * sin(theta) * sin(fi);
				koord3[2] = R * cos(fi);

				koord4[0] = R * cos(theta) * sin(fi + step);
				koord4[1] = R * sin(theta) * sin(fi + step);
				koord4[2] = R * cos(fi + step);
			}

			// iscrtavanje primitiva GL_QUAD_STRIP koji prima uredene cetvorke vrhova kvadrata od glBegin do glEnd
			// -> u svakoj novoj iteraciji unutarnje for petlje se iscrtava jedan kvadratic na tekucoj kruznici koji se nadovezuje na prethodni
			// cime dobivamo STRIP - traku kvadratica uzduz putanje odredene tekucom kruznicom i time iscrtavamo poligone (ovdje kvadrate - QUAD) na povrsini kugle
			glBegin(GL_QUAD_STRIP);
			// crtamo 3d tocke -> otuda oznaka '3'
			// koordinate su realni brojevi s pomicnim zarezom (float) -> otuda oznaka 'f'
			// dajemo gore priredena polja tj. vektore umjesto pojedinacnih argumenata -> otuda oznaka f
			glVertex3fv(koord1);
			// crtamo i normale u trenutnoj tocki
			glNormal3fv(koord1);

			glVertex3fv(koord2);
			glNormal3fv(koord2);

			glVertex3fv(koord3);
			glNormal3fv(koord3);

			glVertex3fv(koord4);
			glNormal3fv(koord4);
			glEnd();
		}
	}
}

void display(void)
{
	glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

	// slijedi blok transformacijskim matrica i matrica modela koje se stavljaju odnosno skidaju sa stoga: od glPushMatrix do glPopMatrix
	glPushMatrix();

	/* Potrebno je dodati kod za iscrtavanje (dijela) Suncevog sustava
	* prema trecem zadatku uputa za ove laboratorijske vjezbe. U uputama
	* je objasnjena "modelview" matrica (transformacija) pomocu koje bi,
	* kombinirajuci razlicite operacije, trebalo implementirati rotacije
	* potrebne za izvodjenje simulacije. Obavezno pogledati napomenu za
	* funkciju mouse()!
	*
	* Koristite sljedece parametre (zadane u VRML laboratorijskoj vjezbi):
	* radijus Sunca = 6.96, Zemlje = 0.63, Mjeseca = 0.17
	* preporuceni step: za Sunce 0.01, za Zemlju i Mjesec 0.1
	* radijus kruzne putanje Zemlje oko Sunca = 14.96
	* radijus kruzne putanje Mjeseca oko Zemlje = 1.0
	*
	* Neposredno prije poziva naredbe za iscrtavanje pojedinih tijela
	* navedite naredbu glCallList () gdje su paramteri te funkcije
	* YELLOWMAT, BLUEMAT ili WHITEMAT, kako bi npr. Suncu sa
	* glCallList (YELLOWMAT) pridodali zutu boju i svojstva doticnog
	* materijala
	*/

	// model Sunca
	// Sunce je definirane zute boje
	glCallList(YELLOWMAT);
	// Sunce je radijusa 6.96 i crta se korakom 0.01
	drawSphere(6.96, 0.01);

	// "Transformacije zajednicke za Zemlju i Mjesec"
	glRotatef(earthYear, 0.0f, 1.0f, 0.0f);		// rotacija Zemlje (odnosno cijelog sustava Zemlja-Mjesec) oko oko Sunca
	glTranslatef(14.96f, 0.0f, 0.0f);		// translacija Zemlje (odnosno cijelog sustava Zemlja-Mjesec) od Sunca za 14.96 (radijus kruzne putanje Zemlja-Sunce)

	// sljedeci blok od glPushMatrix do glPopMatrix cine transformacije koje se samo i to prvo primjenjuju na model Zemlje
	// -> to je konkretno rotacija Zemlje oko svoje osi
	glPushMatrix();
	glRotatef(earthDay, 0.0f, 1.0f, 0.0f);		// rotacija Zemlje oko svoje osi
	// model Zemlje
	// Zemlja je definirane plave boje
	glCallList(BLUEMAT);
	// Zemlja je radijusa 0.63 i crta se korakom 0.1
	drawSphere(0.63, 0.1);
	glPopMatrix();

	// sljedeci blok od glPushMatrix do glPopMatrix cine transformacije koje se samo i to prvo primjenjuju na model Mjeseca
	// -> to su konkretno redom primjene:
	// 1. rotacija Mjeseca oko svoje osi
	// 2. translacija Mjeseca od Zemlje za 1.0
	// 3. rotacija Mjeseca oko Zemlje
	// dalje slijede "Transformacije zajednicke za Zemlju i Mjesec"
	// Napomena: vazno je primjetiti da se navedene transformacije navode upravo suprotnim redoslijedom sto je zbog konvencije OpenGL-a:
	// rezultatne (transformirane) tocke dobivaju se kao Transformacijska matrica * Tocka, umjesto Tocka * Transformacijska matrica
	// -> zbog toga imamo prvo sve transformacijske matrice (ciji ce redoslijed primjene biti obrnut od redoslijeda kojim su tu navedene), a tek na samom kraju sam model (tocke sfere koje dobivamo pozivom drawSphere)
	glPushMatrix();
	glRotatef(moonYear, 0.0f, 1.0f, 0.0f);	// rotacija Mjeseca oko Zemlje
	glTranslatef(1.0f, 0.0f, 0.0f);		// translacija Mjeseca od Zemlje za 1.0 (radijus kruzne putanje Mjesec-Zemlja)
	glRotatef(moonDay, 0.0f, 1.0f, 0.0f);	// rotacija Mjeseca oko svoje osi
	// model Mjeseca
	// Mjesec je definirane bijele boje
	glCallList(WHITEMAT);
	// Mjesec je radijusa 0.17 i crta se korakom 0.1
	drawSphere(0.17, 0.1);
	glPopMatrix();


	glPopMatrix();

	glutSwapBuffers();
}

// funkcija koja vraca trenutno vrijeme u milisekundama
long getCurrentTimeMs()
{
	struct _timeb timebuffer;

	_ftime64_s(&timebuffer);

	return(1000 * timebuffer.time + timebuffer.millitm);
}

/* funkcija koja iz relativnog vremena (razlike izmedju trenutnog
* sistemskog vremena i sistemskog vremena na pocetku simulacije)
* izracunava kuteve rotacije Zemlje i Mjeseca oko vlastitih osi,
* te oko Sunca/Zemlje u svakom trenutku
*/
void spinDisplay(void)
{
	double seconds;

	/* varijabla koja biljezi vrijeme na samom pocetku izvodjenja
	* simulacije
	*/
	static long startTime = -1;

	// varijabla koja biljezi trenutno vrijeme
	long currentTime;

	if (startTime == -1)
		startTime = getCurrentTimeMs();


	currentTime = getCurrentTimeMs();

	// racunanje relativnog vremena proteklog od pocetka simulacije	
	seconds = (double)(currentTime - startTime) / (double)1000;

	//printf("%f\n", seconds);

	/* Funkcija koja iz relativnog vremena (razlike izmedju trenutnog
	* sistemskog vremena i sistemskog vremena na pocetku simulacije)
	* izracunava kuteve rotacije (brzine pomaka) Zemlje i Mjeseca oko
	* vlastitih osi, te oko Sunca/Zemlje u svakom trenutku. Trajanje
	* jednog "mjesecevog dana" jednako je 29.5 zemaljskih dana, a jedne
	* "mjeseceve godine" 27.3 zemaljskih dana. Primjetite da se kutevi
	* rotacije mogu izracunati i bez upotrebe varijable seconds ali ce
	* tada animacija ovisiti o brzini kojom racunalo izvodi program.
	* Znaci u tom slucaju ne dobivamo ispravno vrijeme obilaska planeta
	* oko Sunca/svoje osi.
	*/
	// napomena 1: kraci periodi rotacije znace vece pomake u kutu rotacije za istu vremensku jedinicu
	// -> zbog toga npr.: 1 dan traje 365x manje od 1 god -> u svakoj vremenskoj jedinici treba prijeci 365x veci pomak kuta da bi konacno presao cijeli krug 2*PI
	// napomena 2: mnozimo s 10x manjim vrijednostima jer bi inace jedan ciklus rotacije Zemlje oko Sunca traje 6 sec tj. animacija bi se 10x brze odvila sto je prebrzo (ljepsa je animacija gdje za 60 s Zemlja napravi jedan obilazak oko Sunca)
	// -> zbog toga npr.: *6 umjesto *60 u izracunu earthYear
	// napomena 3: priblizne vrijednosti su uzete iz prethodne vjezbe o VRML-u
	earthDay = seconds * 2190;// 1 dan = 1 god / 365 -> 60 * 365 = 21900 sec
	earthYear = seconds * 6;// 1 god -> 60 sec
	moonDay = seconds * 74.23729;// 1 dan = 29.5 zemljana dana -> 21900 / 29.5 = 742.3729 sec
	moonYear = seconds * 80.21978;// 1 god = 27.3 zemljana dana -> 21900 / 27.3 = 802.1978 sec


	// "oznaka" koja kaze da je prozor potrebno ponovno iscrtati
	glutPostRedisplay();
}

/* Callback funkcija koja se poziva kad se pritisne ili otpusti tipka
* misa. U ovom se konkretno slucaju pritiskom na lijevu tipku misa
* pokrece izvodjenje simulacije, a na desnu tipku zaustavlja njeno
* izvodjenje. Vazno je primjetiti kako se
* funkcija spinDisplay() definira kao funkcija koja ce se stalno
* pozivati (ukoliko nema drugih dogadjaja, npr. mijenjanja velicine
* prozora), i to u svakom "frame-u" (cime cemo postici da se za svaki
* "frame" mogu izracunati novi kutevi rotacije Zemlje i Mjeseca)
*/
void mouse(int button, int state, int x, int y)
{
	switch (button) {
	case GLUT_LEFT_BUTTON:
		if (state == GLUT_DOWN)
			// kad se pritisne lijeva tipka, poceti izracunavati kut rotacije
			glutIdleFunc(spinDisplay);
		break;
	case GLUT_RIGHT_BUTTON:
		if (state == GLUT_DOWN) {
			// kad se pritisne desna tipka, privremeno zaustaviti prikaz simulacije
			glutIdleFunc(NULL);
		}
		break;
	default:
		break;
	}
}

int main(int argc, char** argv)
{
	glutInit(&argc, argv);
	// koristenje dvostrukog spremnika
	glutInitDisplayMode(GLUT_DOUBLE | GLUT_RGB);
	glutInitWindowSize(600, 600);
	glutInitWindowPosition(0, 0);
	glutCreateWindow(argv[0]);
	init();
	glutDisplayFunc(display);
	glutReshapeFunc(reshape);
	// registracija callback funkcije mouse 
	glutMouseFunc(mouse);
	glutMainLoop();
	return 0;
}

