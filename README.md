# OpenGL API Drawing Animation Model and View Transformation Projection

Implemented in C.

Source code is in "izvorniKod" folder and executable code (.exe file) in "izvrsniKod" folder. Microsoft Visual Studio C project in "MSprojekt" folder.

Results are listed and described in "ResultsReport.pdf".

My lab assignment in Introduction to Virtual Environments, FER, Zagreb.

Task descriptions in "TaskSpecification.pdf".

Created: 2020